import type { VueI18n } from 'vue-i18n'
import type { NuxtI18nRoutingCustomProperties } from 'C:/Users/21595/Desktop/yilian-web/node_modules/@nuxtjs/i18n/dist/runtime/types'
import type { I18nRoutingCustomProperties } from 'C:/Users/21595/Desktop/yilian-web/node_modules/vue-i18n-routing/dist/vue-i18n'
import 'C:/Users/21595/Desktop/yilian-web/node_modules/vue-i18n-routing/dist/vue'
declare module '#app' {
  interface NuxtApp {
    $i18n: VueI18n & NuxtI18nRoutingCustomProperties & I18nRoutingCustomProperties
  }
}
declare module 'nuxt/dist/app/nuxt' {
  interface NuxtApp {
    $i18n: VueI18n & NuxtI18nRoutingCustomProperties & I18nRoutingCustomProperties
  }
}